# Doxilog

Doxilog - Analysis of the difficulty of literary texts

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: GPLv3

## Settings

Moved to [settings](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

## Basic Commands

### Start application

      $ docker-compose -f local.yml up --build

### Setting Up Your Users

- To access to the admin panel

- To create a **superuser account**, use this command:

      $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

### Launch tests

      $ docker-compose -f local.yml exec django /entrypoint coverage run -m pytest doxilog/nouslisons/tests

### Url access

Go to [application](http://localhost:8000/nouslisons/)

Go to [admin panel](http://localhost:8000/admin/nouslisons/)

Go to [source code](https://gitlab.com/heg-arc/doxilog/doxilog.git)
