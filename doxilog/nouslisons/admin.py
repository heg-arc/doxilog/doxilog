from django.contrib import admin

from doxilog.nouslisons.models import Word


@admin.register(Word)
class WordAdmin(admin.ModelAdmin):
    model = Word
    search_fields = ("word",)

    list_display = ("original_language", "word")
    ordering = ("original_language", "word")

    list_filter = ("original_language", "tag")
