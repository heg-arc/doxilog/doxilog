from django.urls import path

from . import views

app_name = "nouslisons"

urlpatterns = [
    path("", views.text_analyse, name="text_analyse"),
    path("result", views.text_result, name="text_result"),
    # No access to data loader in the main site
    # path("data", views.data_loading, name="data_load"),
]
