from django.shortcuts import redirect, render
from django.utils import translation
from langdetect import detect

from .treatment.data_loader import extract_data_from_xml
from .treatment.text_processing import process_text
from .treatment.words_tooltips import add_tooltips


def text_analyse(request):
    # Analyse a text
    if request.method == "POST":
        text = request.POST["textarea"]
        request.session["analyzed_text"] = text
        return redirect("nouslisons:text_result")

    # Get session value, display banner if language is not supported
    error_message = request.session.get("error_message", False)
    request.session["error_message"] = False

    language = request.session.get("language", "")
    request.session["language"] = ""
    text = ""

    # Check if edit parameter is present in URL
    edit_text = request.GET.get("edit", False)
    if edit_text:
        # Retrieve the text from the session and pre-fill textarea
        text = request.session.get("analyzed_text", "")

    context = {"error_message": error_message, "language": language, "text": text}
    return render(request, "nouslisons/textanalyse.html", context)


def text_result(request):
    # Take the language of the application to display the right translation
    trad_language = translation.get_language()

    text = request.session.get("analyzed_text")
    try:
        processed_data = process_text(text, trad_language)
    except TypeError:
        # The language is not supported in the application
        error_message = True
        language = detect(text)

        # Store values in session to display error message
        request.session["error_message"] = error_message
        request.session["language"] = language

        # Redirect to text_analysis view
        return redirect("nouslisons:text_analyse")

    # Add tooltips specifications for highlighting uncommon words in the textarea
    highlighted_text = add_tooltips(processed_data["not_ffi_tooltips"], text, trad_language)

    context = {
        "highlighted_text": highlighted_text,
        "processed_data": processed_data,
    }

    return render(request, "nouslisons/textresult.html", context)


def data_loading(request):
    if request.method == "POST":
        data_file = request.FILES["data_file"]

        # Extension file should be .xml
        if data_file is not None and data_file.name.endswith(".xml"):
            # Extract data form xml file
            count_added = extract_data_from_xml(data_file)
            context = {"count_added": count_added}
            return render(request, "nouslisons/dataloading.html", context)

    return render(request, "nouslisons/dataloading.html")
