from django.apps import AppConfig


class NouslisonsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "doxilog.nouslisons"
