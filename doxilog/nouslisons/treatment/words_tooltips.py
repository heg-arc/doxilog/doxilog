import re


# Translation didn't work with makemessages, needed to do it manually
def get_definition_translation(trad_language):
    if trad_language == "fr":
        return "Définition"
    if trad_language == "es":
        return "Definición"
    # Default in English
    else:
        return "Definition"


# Translation didn't work with makemessages, needed to do it manually
def get_no_definition_translation(trad_language):
    if trad_language == "fr":
        return "Aucune définition pour ce mot"
    if trad_language == "es":
        return "No hay definición para esta palabra"
    # Default in English
    else:
        return "No definition for this word"


# Translation didn't work with makemessages, needed to do it manually
def get_translate_translation(trad_language):
    if trad_language == "fr":
        return "Traduction"
    if trad_language == "es":
        return "Traducción"
    # Default in English
    else:
        return "Translate"


def add_tooltips(not_ffi_tooltips, text, trad_language):
    for key, word_value in not_ffi_tooltips.items():
        title = key

        # The infinitive is added for all word
        # if key and infinitive are different this is a conjugated verb
        if str(key) != str(word_value["infinitive"]) and word_value["infinitive"] is not None:
            title = word_value["infinitive"]

        word_replace = '<span class="highlight" id="span" aria-describedby="tooltip">' + key + "</span>"

        tooltip_html = '<div id="tooltip" role="tooltip">'
        tooltip_html += '<p class="word-tag tooltip-content">' + title.lower()

        if word_value["tag"]:
            tooltip_html += ' <i class="tag">' + word_value["tag"] + "</i>"

        tooltip_html += "</p>"
        tooltip_html += '<div class="tooltip-divider"></div>'

        if word_value["definitions"]:
            trad_definition = get_definition_translation(trad_language)
            tooltip_html += '<p class="sub-title">' + trad_definition + "</p>"
            tooltip_html += '<ul class="definition-list">'
            for definition in word_value["definitions"]:
                tooltip_html += '<li class="word-definition">' + definition + "</li>"
            tooltip_html += "</ul>"
        else:
            trad_no_definition = get_no_definition_translation(trad_language)
            tooltip_html += '<p><i class="tag">' + trad_no_definition + "</i></p>"

        if word_value["word_translate"]:
            tooltip_html += '<div class="tooltip-divider"></div>'
            trad_translate = get_translate_translation(trad_language)
            tooltip_html += '<p class="sub-title">' + trad_translate + "</p>"
            tooltip_html += '<p class="traduction">' + word_value["word_translate"] + "</p>"

        tooltip_html += '<div id="arrow" data-popper-arrow></div>'
        tooltip_html += "</div>"

        # Using regex to find word occurrences with word boundaries
        regex_pattern = r"(?<![a-zA-ZÀ-ÿ0-9_])" + re.escape(key) + r"(?![a-zA-ZÀ-ÿ0-9_])"
        text = re.sub(regex_pattern, word_replace + tooltip_html, text)

    return text
