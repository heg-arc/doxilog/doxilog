import locale

import spacy.util
from langdetect import detect
from nltk.stem import SnowballStemmer

from doxilog.nouslisons.models import Word

baseRates = [75, 70, 67, 64, 58, -1]
ageLabels = ["6 à 9 ans", "9/10 ans", "10/11 ans", "11/12 ans", "12/13 ans", "13 ans et +"]
lvlLabels = ["A1", "A2", "B1", "B2", "C1", "C2"]

FR = "fr"
ES = "es"
EN = "en"
cores = ["fr_core_news_sm", "es_core_news_sm", "en_core_web_sm"]

tags_propn = ["nom propre", "prénom", "nom de famille", "nom commun", "proper noun", "sustantivo propio"]

word_exception = ["aujourd'hui", "o'clock"]

file_path = "doxilog/static/txt/"


# Load language model
def load_cores():
    for core in cores:
        spacy.load(core)


# Pre-load language model for performances
load_cores()


# Define Stemmer language and initialize lists and dict
def init_lists(language):
    if language == ES:
        language_stemmer = "spanish"
    elif language == EN:
        language_stemmer = "english"
    else:
        language_stemmer = "french"

    stemmer = SnowballStemmer(language_stemmer)
    all_unique_words_stemm = {}
    proper_noun = set()
    ffi_words = []
    not_ffi_words = []
    not_ffi_tooltips = {}
    return stemmer, all_unique_words_stemm, proper_noun, ffi_words, not_ffi_words, not_ffi_tooltips


def encoding_cleaning(text):
    text = text.replace("’", "'")  # replace the curved apostrophe by a single apostrophe
    text = text.replace("œ", "oe")  # replace the ligature 'œ' by 'oe'
    text = text.replace("-", " ")  # replace dashes with a space
    text = text.replace("\n", " ")  # replace line breaks with a space
    return text


def load_model(language):
    if language == EN:
        nlp = spacy.load("en_core_web_sm")
        return nlp
    elif language == FR or language == ES:
        nlp = spacy.load(f"{language}_core_news_sm")
        return nlp
    else:
        return ""


def lemming(token):
    # Words with 'VERB' and 'AUX' tag are lemming
    if token.pos_ == "VERB" or token.pos_ == "AUX":
        return token.lemma_
    else:
        return token.text


# Exception : Contraction treated manually
def lemming_en_correction(word_lemma):
    # Transformation of "'ve" (have) in english
    if word_lemma == "'ve":
        word_lemma = "have"

    # Transformation of for "n't" (not) in english
    if word_lemma == "n't":
        word_lemma = "not"

    return word_lemma


# Take root of the word
def stemming(stemmer, token):
    stemmed_word = stemmer.stem(token)
    return stemmed_word


def word_with_apostrophe(word):
    # Exception for word that should be taken with '
    if word in word_exception:
        return word

    # Word with ' check if it has more than one letter before
    if word.count("'") == 1 and len(word.split("'")[0]) > 1:
        # Clean ' of the word
        word_clean = word.replace("'", "")
        return word_clean
    else:
        # Has no ' word that should not be in the list
        return ""


def extract_words_from_txt(file_path_list_ffl):
    words = []
    with open(file_path_list_ffl) as file:
        for line in file:
            word = line.strip()
            words.append(word)
    return words


def get_translation_from_db(word_obj, trad_language):
    if trad_language == "fr":
        return word_obj.fr_traduction
    elif trad_language == "es":
        return word_obj.es_traduction
    elif trad_language == "en":
        return word_obj.en_traduction
    elif trad_language == "ru":
        return word_obj.ru_traduction
    elif trad_language == "it":
        return word_obj.it_traduction
    elif trad_language == "de":
        return word_obj.de_traduction
    elif trad_language == "ar":
        return word_obj.ar_traduction
    elif trad_language == "ja":
        return word_obj.ja_traduction
    elif trad_language == "zh":
        return word_obj.zh_traduction
    else:
        return word_obj.fr_traduction


def calculate_age_level(percentage):
    age = None
    level = None

    for rate, age_label, level_label in zip(baseRates, ageLabels, lvlLabels):
        if percentage >= rate:
            age = age_label
            level = level_label
            break

    return age, level


def process_text(text, trad_language):
    # Encoding cleaning
    text_encode = encoding_cleaning(text)

    # Language detection with langdetect
    language = detect(text_encode)

    # Load model spacy
    nlp = load_model(language)
    doc = nlp(text_encode)

    # Initialisation list and dict
    stemmer, all_unique_words_stemm, proper_noun, ffi_words, not_ffi_words, not_ffi_tooltips = init_lists(language)

    for token in doc:
        # Lemming VERB and AUX
        word_lemma = lemming(token)

        # Correction of lemming for english word
        if language == "en":
            word_lemma = lemming_en_correction(word_lemma)

        # Tagged 'Proper Noun' and first letter is upper
        if token.pos_ == "PROPN" and token.text[0].isupper():
            print(str(token))
            # Check if 'PROPN' is in database
            word = Word.objects.filter(word=token.text).first()
            if word is None:
                print(str(token))
                word = Word.objects.filter(word=token.text.lower()).first()
            if word is not None:
                if word.tag in tags_propn:
                    # If it is a PROPN in db add to proper_noun list
                    proper_noun.add(token.text)
                    continue
                else:
                    # Spacy tag was not correct, check if it's a verb
                    if word.tag == "verbe":
                        #  Take the infinitive of the verb and use it as token.text
                        word_lemma = word.infinitive_verb
                    # Process continue with treatment
            else:
                # It is not in database that's a 'PROPN'
                proper_noun.add(token.text)
                continue

        # Stemming
        word_stem = stemming(stemmer, word_lemma)

        # If word is not alpha
        if not word_stem.isalpha():
            # Clean words with apostrophe
            word_stem_clean = word_with_apostrophe(word_stem)

            # Word that should be in the list after cleaning
            if not word_stem_clean == "":
                # Check if the key already exists
                if word_stem_clean.lower() in all_unique_words_stemm:
                    # The key already exists, add the token.text to the word_highlight list
                    all_unique_words_stemm[word_stem_clean.lower()]["word_highlight"].append(token.text)
                else:
                    # Create new dict for word and add it to all_unique_words
                    dict_words_stemm = {
                        "word_lemma": word_lemma.lower(),
                        "word_highlight": [token.text],
                        "original_word": token.text,
                    }
                    all_unique_words_stemm[word_stem_clean.lower()] = dict_words_stemm
        else:
            # No cleaning needed
            # Check if the key already exists
            if word_stem.lower() in all_unique_words_stemm:
                # The key already exists, add the token.text to the word_highlight list
                all_unique_words_stemm[word_stem.lower()]["word_highlight"].append(token.text)
            else:
                # Create new dict for word and add it to all_unique_words
                dict_words_stemm = {
                    "word_lemma": word_lemma.lower(),
                    "word_highlight": [token.text],
                    "original_word": token.text,
                }
                all_unique_words_stemm[word_stem.lower()] = dict_words_stemm

    # Extract txt
    file_path_list_ffl = file_path + language + "_list_ffl.txt"
    word_list_ffl = extract_words_from_txt(file_path_list_ffl)

    # Check if ffl and add the full word present in text to list
    for key, value in all_unique_words_stemm.items():
        if key in word_list_ffl:
            # Common word
            ffi_words.append(value["word_lemma"])
        else:
            # Uncommon word
            not_ffi_words.append(value["word_lemma"])

            for word_tooltips in value["word_highlight"]:
                try:
                    # Get word object for each word that should be highlighted
                    word_obj = Word.objects.get(original_language=language, word=word_tooltips.lower())

                    infinitive_word = word_obj.word

                    # Word with no infinitive verb keep itself has infinitive word
                    if word_obj.infinitive_verb != "":
                        # Get the infinitive of the word to display the infinitive form in tooltip
                        word_obj = Word.objects.get(original_language=language, word=word_obj.infinitive_verb)
                        infinitive_word = word_obj.word

                    # Get the right translation with the language of the application
                    word_translate = get_translation_from_db(word_obj, trad_language)

                    # Definitions are separate with ||
                    split_definition = word_obj.definition.split("||")
                    definition_list = [item.strip() for item in split_definition]

                    # Some dict have [''], define None to definitions if there is nothing
                    # Needed to display "No definition" in tooltips popper
                    if all(not definition for definition in definition_list):
                        word_dict = {
                            "definitions": None,
                            "tag": word_obj.tag,
                            "word_translate": word_translate,
                            "infinitive": infinitive_word,
                        }
                    else:
                        word_dict = {
                            "definitions": definition_list,
                            "tag": word_obj.tag,
                            "word_translate": word_translate,
                            "infinitive": infinitive_word,
                        }
                    not_ffi_tooltips[word_tooltips] = word_dict

                except Word.DoesNotExist:
                    # No word in database
                    word_dict = {"definitions": None, "tag": None, "word_translate": None, "infinitive": None}
                    not_ffi_tooltips[word_tooltips] = word_dict

    # Percentage of ffi word
    percentage = int((len(ffi_words) / len(all_unique_words_stemm)) * 100)

    # Calculate age and level
    age, level = calculate_age_level(percentage)

    # Sort list only in FR
    loc_language = "fr_FR.UTF-8"
    locale.setlocale(locale.LC_ALL, loc_language)

    all_unique_words_stemm = sorted(all_unique_words_stemm.values(), key=lambda x: locale.strxfrm(x["word_lemma"]))
    proper_noun = sorted(proper_noun, key=locale.strxfrm)
    ffi_words = sorted(ffi_words, key=locale.strxfrm)
    not_ffi_words = sorted(not_ffi_words, key=locale.strxfrm)

    return {
        "language": language,
        "age": age,
        "level": level,
        "percentage": percentage,
        "all_unique_words_stemm": all_unique_words_stemm,
        "proper_noun": proper_noun,
        "ffi_words": ffi_words,
        "not_ffi_words": not_ffi_words,
        "not_ffi_tooltips": not_ffi_tooltips,
    }
