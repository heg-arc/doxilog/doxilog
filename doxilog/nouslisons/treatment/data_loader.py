import re
import xml.etree.ElementTree as ET

from ..models import Word


def extract_data_from_xml(xml_data):
    count_added = 0
    with xml_data.open() as f:
        is_inside_page = False
        lines = ""
        line = f.readline().decode("utf-8")
        while line:
            if "<page>" in line:
                is_inside_page = True

            if is_inside_page:
                lines += line

            if "</page>" in line:
                is_inside_page = False

                # Treatment return 1 if word is added
                count_page = define_extraction_language(xml_data.name, lines)
                count_added += count_page

                # Help to follow insertion in terminal
                if count_added % 25000 == 0 and not count_added == 0:
                    print(count_added)
                lines = ""
            line = f.readline().decode("utf-8")

    return count_added


def define_extraction_language(xml_name, lines):
    count_return = 0

    # Select extraction in correct language if the xml_name contain the language + 'wiktionary'
    if "frwiktionary" in xml_name:
        count_return = fr_extraction(lines)
    elif "enwiktionary" in xml_name:
        count_return = en_extraction(lines)
    elif "eswiktionary" in xml_name:
        count_return = es_extraction(lines)
    return count_return


def definitions_cleaning(definition_matches, word_data):
    if definition_matches:
        definitions = []
        for match in definition_matches:
            # Allows to exclude lines with an # inside like " # [[baguette#fr|Baguette]]. "
            if "#" in match:
                continue

            # Regular expression to extract the 2nd terms between [[ and ]]
            # Regular expression to remove elements between {{ and }} including
            # "des [[mots clés|termes]] et des {{éléments}}" become "des termes et des éléments"
            definition = re.sub(r"\[\[([^\[\]\|]*?\|)?(.*?)\]\]", r"\2", match)
            definition = re.sub(r"\{\{.*?\}\}", "", definition)

            # Delete the '' character used in conjugated verbs
            definition = definition.replace("''", "")

            # Remove first character if not alphabetical
            if definition and not definition[0].isalpha():
                definition = definition[1:]

            # Remove spaces at the beginning of the definition
            definition = definition.lstrip()

            # Allows 2 definitions maximum
            definitions.append(definition.strip())
            if len(definitions) == 2:
                break
        word_data["definition"] = " ||".join(definitions)
        return 1
    else:
        # No definition
        return 0


def insert_data(word_data):
    # Inserting data into the model
    word = Word(
        original_language=word_data["original_language"],
        word=word_data["word"],
        infinitive_verb=word_data["infinitif"],
        tag=word_data["tag"],
        definition=word_data.get("definition", ""),
        fr_traduction=word_data.get("fr_traduction", ""),
        es_traduction=word_data.get("es_traduction", ""),
        en_traduction=word_data.get("en_traduction", ""),
        ru_traduction=word_data.get("ru_traduction", ""),
        it_traduction=word_data.get("it_traduction", ""),
        de_traduction=word_data.get("de_traduction", ""),
        ar_traduction=word_data.get("ar_traduction", ""),
        ja_traduction=word_data.get("ja_traduction", ""),
        zh_traduction=word_data.get("zh_traduction", ""),
    )
    word.save()
    return 1


def fr_extraction(lines):
    e = ET.fromstring(lines)

    word_data = {}

    # Word title extraction
    title_tag = e.find("title").text
    if title_tag.isalpha() and " " not in title_tag:
        word_data["word"] = title_tag
    else:
        return 0

    already = Word.objects.filter(word=title_tag, original_language="fr").first()
    if already is not None:
        return 0

    # Extraction of text tag in revision to take tag, definitions and translations
    text_balise = e.find("revision/text").text

    if not text_balise:
        # Word not added
        return 0

    # Check if language of the word is 'fr'
    language_tag = "fr"
    language_match = re.search(r"{{langue\|([^|}]+)}}", text_balise)
    if language_match and language_match.group(1) == language_tag:
        word_data["original_language"] = language_match.group(1)
    else:
        # Don't add if not 'fr'
        return 0

    # Tag extraction
    # Example : === {{S|nom|fr}} ===
    tag_match = re.search(r"=== {{S\|([^|{}]*)(?:\|(?:(?!}}).)*)?\|fr(?:\|(?:.*?))?}} ===", text_balise)
    if tag_match:
        word_data["tag"] = tag_match.group(1).lower()
    else:
        return 0

    # Extraction of the infinitive if it is a verb
    if tag_match.group(1) == "verbe":
        infinitive_match = re.search(r"{{fr-verbe-flexion\|([^|}]+)", text_balise)
        if infinitive_match:
            infinitive = infinitive_match.group(1)
            # If the capture contains an equal, take the next group
            if "=" in infinitive:
                # Take a second capture to obtain the next group
                infinitive_match = re.search(r"{{fr-verbe-flexion\|(?:[^|}]+)\|([^|}]+)", text_balise)
                if infinitive_match:
                    infinitive = infinitive_match.group(1)
            word_data["infinitif"] = infinitive
        else:
            word_data["infinitif"] = ""
    else:
        word_data["infinitif"] = ""

    # Extraction of the first 2 definitions
    definition_matches = []
    for match in re.finditer(r"^#+\s*([^*\n]+)(?!\*)$", text_balise, flags=re.MULTILINE):
        definition_matches.append(match.group(1))
        if len(definition_matches) == 2:
            break

    # Clean definitions
    have_definition = definitions_cleaning(definition_matches, word_data)
    if have_definition == 0:
        # If there is no definition, insert_data with tag and infinitve only
        return insert_data(word_data)

    # Extracting translations for specified languages
    target_languages = ["fr", "es", "en", "ru", "it", "de", "ar", "ja", "zh"]

    # Get all translation inside "trad-début" and "trad-fin"
    translation_matches = re.findall(
        r"\{\{trad-début[^{}]*\}\}[\s\S]*?\{\{trad-fin\}\}", text_balise, flags=re.DOTALL
    )[:2]

    for match in translation_matches:
        # Example translation : * {{T|de}} : {{trad+|de|Aufnahme|f}}, {{trad+|de|Empfang|m}}
        translations = re.findall(r"\{\{trad[-+]?\|([a-z]{2})\|([^|}]+)(?:\|(?:.*?(?:tr=|R=)([^|}]+))?)?", match)
        translation_counts = {}

        for tag, translation, tr_value in translations:
            if tag in target_languages:
                if tag not in translation_counts:
                    translation_counts[tag] = 0

                # Limit number of translate at 3
                if translation_counts[tag] < 3:
                    # Check if translation exist in dict
                    if tag + "_traduction" in word_data:
                        # For ar, ja, zh and ru, take tr value (word write in latin)
                        if tr_value:
                            translation = f"{translation} ({tr_value})"
                        if translation not in word_data[tag + "_traduction"]:
                            word_data[tag + "_traduction"].append(translation)
                            translation_counts[tag] += 1
                    else:
                        # For ar, ja, zh and ru, take tr value (word write in latin)
                        if tr_value:
                            translation = f"{translation} ({tr_value})"
                        word_data[tag + "_traduction"] = [translation]
                        translation_counts[tag] += 1

    # Concatenate translations with ", " as separator
    for key, value in word_data.items():
        if isinstance(value, list):
            word_data[key] = ", ".join(value)

    return insert_data(word_data)


def en_extraction(lines):
    e = ET.fromstring(lines)

    word_data = {}

    # Word title extraction
    title_tag = e.find("title").text
    if title_tag.isalpha() and " " not in title_tag:
        word_data["word"] = title_tag
    else:
        return 0

    already = Word.objects.filter(word=title_tag, original_language="en").first()
    if already is not None:
        return 0

    # Extraction of text tag in revision to take tag, definitions and translations
    text_balise = e.find("revision/text").text

    if not text_balise:
        # Word not added
        return 0

    # Check if language of the word is 'en'
    language_tag = "English"
    language_match = re.search(rf"=={language_tag}==", text_balise)
    if language_match:
        word_data["original_language"] = "en"
    else:
        # Don't add if not 'en'
        return 0

    # Tag extraction
    # Example : ===Adjective===
    #           {{en-adj|er}}
    tag_match = re.search(r"={3,4}([^=]+)={3,4}\n{{en-(\w+)(?:\|[^{}]+)?}}", text_balise)
    if tag_match:
        word_data["tag"] = tag_match.group(1).lower()
    else:
        # Don't add if it has no tag
        return 0

    # Extraction of the infinitive if it is a verb
    # Finding the line containing "{{head|en|verb form}}"
    verb_form_line = r"\{\{head\|en\|verb form\}\}\n\n"
    # Check if the line below has this format :
    # "# {{inflection of|en|outsource||pres|part}}" or "# {{en-simple past of|fall}}"
    information_line = r"# \{\{[^|}]+ of\|([^|}]+)(?:\|[^|}]*)*\}\}"
    infinitive_search = re.search(verb_form_line + information_line, text_balise)

    if infinitive_search:
        # Group 1 is en like in : {{inflection of|en|
        if infinitive_search.group(1) == "en":
            # Do another search to take the infinitive form
            # Example : # {{present participle of|en|hide||nocat=1}}
            information_line = r"# \{\{[^|}]+ of\|en\|([^|}]+)(?:\|[^|}]*)*\}\}"

        # If no result, we try this case : # {{en-third-person singular of|die}}
        if not information_line:
            information_line = r"# \{\{en-[^|}]+ of\|([^|}]+)(?:\|[^|}]*)*\}\}"

            # Example : {{head|en|verb form}}
            #
            #           # {{en-third-person singular of|pie}}
            infinitive_search = re.search(verb_form_line + information_line, text_balise)
            word_data["infinitif"] = infinitive_search.group(1)
        else:
            word_data["infinitif"] = infinitive_search.group(1)
    else:
        word_data["infinitif"] = ""

    # Extraction of the first 2 definitions
    definition_matches = []
    for match in re.finditer(r"^#+\s*([^*\n:]+)(?!(\*|.*#:))$", text_balise, flags=re.MULTILINE):
        # Exclude rows like this : # {{inflection of|en|outsource||pres|part}}
        if "{{" not in match.group(1):
            definition_matches.append(match.group(1))
        if len(definition_matches) == 2:
            break

    # Clean definitions
    has_definition = definitions_cleaning(definition_matches, word_data)
    if has_definition == 0:
        # If there is no definition, insert_data with tag and infinitive only
        return insert_data(word_data)

    # Extracting translations for specified languages
    target_languages = ["fr", "es", "en", "ru", "it", "de", "ar", "ja", "cmn"]

    # Get all translation inside "trad-top" and "trad-bottom"
    translation_matches = re.findall(
        r"\{\{trans-top[^{}]*\}\}[\s\S]*?\{\{trans-bottom\}\}", text_balise, flags=re.DOTALL
    )[:2]

    for match in translation_matches:
        # Example translation : * French: {{t+|fr|externalisation|f}}, {{t+|fr|sous-traitance|f}}
        translations = re.findall(
            r"\{\{t[-+]?\|([a-z]{2,3})\|(?:\[\[)?([^|}]+)(?:\|(?:.*?(?:tr=|R=)([^|}]+))?)?", match
        )
        translation_counts = {}

        for tag, translation, tr_value in translations:
            if tag in target_languages:
                # Transform cmn in zh, Chinese have cmn example : Mandarin: {{t|cmn|[[標準|標準的]]|tr=biāozhǔn de}}
                if tag == "cmn":
                    tag = "zh"

                if tag not in translation_counts:
                    translation_counts[tag] = 0

                # Limit number of translate at 3
                if translation_counts[tag] < 3:
                    if tag + "_traduction" in word_data:
                        # For ar, ja, zh and ru, take tr value (word write in latin)
                        if tr_value:
                            translation = f"{translation} ({tr_value})"
                        if translation not in word_data[tag + "_traduction"]:
                            word_data[tag + "_traduction"].append(translation)
                            translation_counts[tag] += 1
                    else:
                        # For ar, ja, zh and ru, take tr value (word write in latin)
                        if tr_value:
                            translation = f"{translation} ({tr_value})"
                        word_data[tag + "_traduction"] = [translation]
                        translation_counts[tag] += 1

    # Concatenate translations with ", " as separator
    for key, value in word_data.items():
        if isinstance(value, list):
            word_data[key] = ", ".join(value)

    return insert_data(word_data)


def es_extraction(lines):
    e = ET.fromstring(lines)

    word_data = {}

    # Word title extraction
    title_tag = e.find("title").text
    if title_tag.isalpha() and " " not in title_tag:
        word_data["word"] = title_tag
    else:
        return 0

    already = Word.objects.filter(word=title_tag, original_language="es").first()
    if already is not None:
        return 0

    # Extraction of text tag in revision to take tag, definitions and translations
    text_balise = e.find("revision/text").text

    if not text_balise:
        # Word not added
        return 0

    # Check if language of the word is 'es'
    language_tag = "es"
    language_match = re.search(r"{{lengua\|([^|}]+)}}", text_balise)
    if language_match and language_match.group(1) == language_tag:
        word_data["original_language"] = language_match.group(1)
    else:
        # Don't add if not 'es'
        return 0

    # Tag extraction, conjugated verbs have a different format, we have to extract them manually
    # If no match check if it is conjugated verb. Example : === {{sustantivo masculino|es}} ===
    tag_match = re.search(r"=== \{\{([^|{}]+)\|es\}\} ===", text_balise)
    if tag_match:
        word_data["tag"] = tag_match.group(1).lower()
    else:
        tag_verb = re.search(r"=== Forma verbal ===", text_balise)
        if tag_verb:
            word_data["tag"] = "forma verbal"
        else:
            # Don't has tag
            return 0

    # Extraction of the infinitive if it is a verb
    # Example for tramar : {{forma verbo|tramar|p=3s|t=presente|m=indicativo}}
    if word_data["tag"] == "forma verbal":
        infinitive_match = re.search(r"{{forma verbo\|([^|}]+)", text_balise)
        if infinitive_match:
            infinitive = infinitive_match.group(1)
            word_data["infinitif"] = infinitive
        else:
            word_data["infinitif"] = ""
    else:
        word_data["infinitif"] = ""

    # Extraction of the first 2 definitions
    # Example match definition : ;1 {{biología}}: Diferencia de potencial
    definition_matches = []
    for match in re.finditer(r"^;\d:?\s*([^\n]+)", text_balise, flags=re.MULTILINE):
        definition_matches.append(match.group(1))
        if len(definition_matches) == 2:
            break

    # Clean definitions
    has_definition = definitions_cleaning(definition_matches, word_data)
    if has_definition == 0:
        # If there is no definition, insert_data with tag and infinitive only
        return insert_data(word_data)

    # Extracting translations for specified languages
    target_languages = ["fr", "es", "en", "ru", "it", "de", "ar", "ja", "zh"]

    # Get all translation inside "trad-arriba" and "trad-abajo"
    translation_matches = re.findall(
        r"\{\{trad-arriba[^{}]*\}\}[\s\S]*?\{\{trad-abajo\}\}", text_balise, flags=re.DOTALL
    )[:1]

    for matches in translation_matches:
        # Spanish file only has 1 translation container per word
        # Example translation : {{t+|fr|1|igloo|m|,|iglou}}
        translations = re.findall(r"\{{t[+-]?\|([a-z]{2})\|(?:\d+-?\d?\|)?([^}]+)}}", matches)

        # Definitions have many different cases, manual processing
        for match in translations:
            tag = match[0]  # The tag
            if tag in target_languages:
                translation = match[1]  # The raw translation
                # "chose|f" is a page display, we exclude it as a translation
                if translation == "chose|f":
                    continue

                # Remove digits and what follows up to a |
                translation = re.sub(r"[\d?]+-?,?(\d?\|)?", "", translation)

                # Check if it is |n| |m| or |f| and delete them if so
                if "|m|" in translation:
                    translation = translation.replace("|m", "")
                if "|f|" in translation:
                    translation = translation.replace("|f", "")
                if "|n|" in translation:
                    translation = translation.replace("|n", "")
                translation = re.sub(r"\|([fmn])$", "", translation)

                # Replace |tr=... or |tr|... with (...), delete the | remaining
                translation = re.sub(r"\|tr[|=][^|]+", lambda x: f" ({x.group(0)[4:]}) ", translation)
                translation = translation.replace("|,|", ", ")
                translation = re.sub(r" [a-zA-Z]{1,2}\|", "", translation)
                translation = translation.replace("|", " ")

                # Special treatment for Japanese formatting 脆弱性 (ぜいじゃくせい, zeijakusei)
                translation = re.sub(r"\(([^)]*),([^)]*)\)", r"(\1/\2)", translation)

                # Remove everything after the third comma (including the comma)
                parts = translation.split(",")
                if len(parts) > 3:
                    translation = ",".join(parts[:3])

                word_data[tag + "_traduction"] = translation

    return insert_data(word_data)
