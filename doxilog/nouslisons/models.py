from django.db import models
from django.utils.translation import gettext_lazy as _


class Word(models.Model):
    original_language = models.CharField(max_length=2)
    word = models.CharField(max_length=100)
    infinitive_verb = models.CharField(max_length=50, blank=True)
    tag = models.CharField(max_length=50)
    definition = models.TextField()
    fr_traduction = models.CharField(max_length=225, blank=True)
    es_traduction = models.CharField(max_length=225, blank=True)
    en_traduction = models.CharField(max_length=225, blank=True)
    ru_traduction = models.CharField(max_length=225, blank=True)
    it_traduction = models.CharField(max_length=225, blank=True)
    de_traduction = models.CharField(max_length=225, blank=True)
    ar_traduction = models.CharField(max_length=225, blank=True)
    ja_traduction = models.CharField(max_length=225, blank=True)
    zh_traduction = models.CharField(max_length=225, blank=True)

    class Meta:
        verbose_name = _("word")
        verbose_name_plural = _("words")
        ordering = ["word"]
        indexes = [
            models.Index(fields=["word"]),
            models.Index(fields=["original_language", "word"]),
        ]
        constraints = [
            models.UniqueConstraint(fields=["original_language", "word"], name="unique_language_word"),
        ]

    def __str__(self):
        return f"{self.word}"
