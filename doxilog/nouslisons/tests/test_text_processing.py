from unittest.mock import Mock

from django.test import TestCase
from nltk import SnowballStemmer

from doxilog.nouslisons.treatment.text_processing import (
    calculate_age_level,
    encoding_cleaning,
    lemming,
    process_text,
    stemming,
    word_with_apostrophe,
)


class TestLemmingTextProcessing(TestCase):
    def test_lemming_verb(self):
        token = Mock(pos_="VERB", text="ran")
        result = lemming(token)

        # Result should be different as the token
        self.assertNotEqual(result, "ran")

    def test_lemming_noun(self):
        token = Mock(pos_="NOUN", text="apple")
        result = lemming(token)

        # Result should be the same as the token
        self.assertEqual(result, "apple")


class TestStemmingTextProcessing(TestCase):
    def test_stemming(self):
        stemmer = SnowballStemmer("english")
        token = "arrive"
        result = stemming(stemmer, token)
        self.assertEqual(result, "arriv")


class TestWordWithApostropheTextProcessing(TestCase):
    def test_word_with_apostrophe_exception(self):
        word = "o'clock"
        result = word_with_apostrophe(word)
        self.assertEqual(result, "o'clock")

    def test_word_with_apostrophe(self):
        with_apostrophe = "lorsqu'"
        without_apostrophe = "lorsqu"
        result = word_with_apostrophe(with_apostrophe)
        self.assertEqual(without_apostrophe, result)


class TestEncodingTextProcessing(TestCase):
    def test_encoding_cleaning(self):
        input_text = "Le cœur de grand-mère s'est éteint !"
        expected_output = "Le coeur de grand mère s'est éteint !"
        cleaned_text = encoding_cleaning(input_text)
        self.assertEqual(cleaned_text, expected_output)


class TestCalculTextProcessing(TestCase):
    def test_calculate_age_level(self):
        percentage1 = 92
        percentage2 = 55
        expected_age1 = "6 à 9 ans"
        expected_age2 = "13 ans et +"
        expected_level1 = "A1"
        expected_level2 = "C2"
        age1, level1 = calculate_age_level(percentage1)
        age2, level2 = calculate_age_level(percentage2)
        self.assertEqual(age1, expected_age1)
        self.assertEqual(level1, expected_level1)
        self.assertEqual(age2, expected_age2)
        self.assertEqual(level2, expected_level2)


class TestProperNounTextProcessing(TestCase):
    def test_process_text_with_proper_noun(self):
        text = "Je suis de Paris et c'est très triste"
        result = process_text(text, "fr")
        self.assertIn("Paris", result["proper_noun"])
