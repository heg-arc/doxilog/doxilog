from django.test import TestCase
from django.urls import resolve, reverse


class TestUrlsTestCase(TestCase):
    def test_home(self):
        assert reverse("nouslisons:text_analyse") == "/nouslisons/"
        assert resolve("/nouslisons/").view_name == "nouslisons:text_analyse"

    def test_result(self):
        assert reverse("nouslisons:text_result") == "/nouslisons/result"
        assert resolve("/nouslisons/result").view_name == "nouslisons:text_result"

    """
    # Data load view is hide
    def test_loader(self):
        assert reverse("nouslisons:data_load") == "/nouslisons/data"
        assert resolve("/nouslisons/data").view_name == "nouslisons:data_load"
    """
