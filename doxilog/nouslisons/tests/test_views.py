from django.test import TestCase
from django.urls import reverse


class TestViews(TestCase):
    def test_text_analyse_language_supported(self):
        response = self.client.post(
            reverse("nouslisons:text_analyse"), {"textarea": "Ceci est un texte."}, follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.client.session["analyzed_text"], "Ceci est un texte.")
